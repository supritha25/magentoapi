$(document).ready(function(){
    $.ajax({
        url: window.location.protocol+"//"+window.location.host+"/"+"api/cart",
        type:"GET",
        dataType: "json",
        data: {  },
        success:function(data){    
          var len = data.products.length;
          $('.label-count').html(len);
          $.each(data.products, function(i,data)
          {
            var div_data = "<li class='product-item'>"+
                           "<div class='grid no-pad product-name-row'>"+
                           "<span class='img-container'>"+
                           "<img src='" + data.thumbnailURL + "'' />"+
                           "</span><span class='product-item-content'>"+
                           "<h5 class='product-title'>"+data.title+"</h5>"+
                           "</span>"+
                           "</div>"+
                           "</li>";
            $(div_data).appendTo(".product-list");
          });
          //$('#warrantiesinner').html(data);
        },error:function(){ 
            //alert("error!!!!");
        }
    }); 
  });