<html lang="en" class="js-tabby"><head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Shopmonk</title>
  <meta name="description" content="Shopmonk">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,500,600,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   
   <link href="https://raw.githubusercontent.com/noizwaves/bootstrap-social-buttons/v1.0.0/social-buttons-3.css" rel="stylesheet"> 
 <script type="text/javascript" src="/assets/jquery.min.js"></script>
  <style>
  
  </style>
</head>
<body>
<!-- <div id="app"> -->
<!--    <div id="app-container"> -->
<!--       <span > -->
         <!-- <div  style="opacity: 1; transition: opacity 0.3s ease-in;"> -->
            <div id="authentication">
              <header class="fixed-nav" id="main-nav">
                <div class="container">
                  <div class="grid no-pad">
                    <div class="logo-container"><a class="nav-btn" href="/brands"><i class="material-icons">menu</i></a>
                      <div class="logo">Shopmonk</div>
                      <div class="tagline" style="width: 867px;">The World's Best Brands. Here.</div>
                    </div>
                    <div class="nav-right">
                      <ul class="icon-nav">
                        <li><a class="nav-btn" href="/wish-list"><i class="material-icons">favorite</i></a></li>    
                        <li><a class="nav-btn" href="/checkout"><i class="material-icons">shopping_cart</i></a></li>
                        <li><a class="nav-btn" href="/profile"><i class="material-icons">account_box</i></a></li>
                        <li class="has-dropdown shown" id="drop">
                          <a href="#" class="nav-btn js-prevent-link">
                            <i class="material-icons md-24">more_horiz</i>
                          </a>
                          <ul class="dropdown" hidden="">
                            <li>
                              <a href="/about">About</a>
                            </li>
                            <li>
                              <a href="/privacy">Privacy Policy</a>
                            </li>
                            <li>
                              <a href="/terms">Terms</a>
                            </li>
                            <li>
                              <a href="/contact">Contact</a>
                            </li>
                            <li>
                              <a href="/logout">Logout</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </header>

              <div class="fixed-nav" id="secondary-nav" style="left: 0px;">
                <div class="container">
                  <div class="grid title-bar">
                    <a class="nav-btn" href="/">
                      <i class="material-icons">arrow_back</i>
                    </a>
                    <div class="title-bar-container">
                      <h2>Login</h2>
                    </div>
                  </div>
                </div>
              </div>

              <div class="checkout-container">
                <section class="content">
                        <div class="grid no-pad">
      <form action="login" method="POST" class="form-block contact-form">
         <input type="hidden" name="_token" value="F18hoPNhvDzyODzHywSHuRxS3Yc9waQWeGhRgndR">
         
         <div class="form-group">
            <label>
                <span>Email</span>
                <input type="email" name="email">
            </label>
         </div>
         <div class="form-group">
            <label>
              <span>Password</span>
              <input type="password" name="password">
           </label>
          </div>
          <div class="action-container">
              <input type="submit" value="Login" class="btn btn-lg">
          </div>
      </form>
       <div class="social-login">
           <p class="or text-center">
               <span>OR</span>
           </p>
           <div class="grid no-pad social-btn-container">
               <div class="col-1-2">
                   <a href="http://demo.shopmonk.com/auth/facebook">
                      <button class="social-btn ghost-btn social-facebook">Sign in with Facebook</button>
                    </a>  
               </div>
               <div class="col-1-2">
                   <a href="http://demo.shopmonk.com/auth/google"><button class="social-btn ghost-btn social-google">Sign in with Google</button></a>
               </div>
           </div>
       </div>
 </div>
                  </section>
              </div>
            </div>
          <!-- </div> -->
<!--       </span> -->
<!--     </div> -->
<!-- </div> -->

<script>
$("#drop").click(function(){
$(".dropdown").toggle();
});
</script>
<script type="text/javascript" src="/assets/static-a8dffa2cf58e8a61d7ec.min.js">
</script>

</body></html>