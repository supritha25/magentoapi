<html lang="en" class="js-tabby"><head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Shopmonk</title>
  <meta name="description" content="Shopmonk">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,500,600,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- <link href="http://demo.shopmonk.com/assets/css/frontend.css" rel="stylesheet">
   <link href="http://demo.shopmonk.com/assets/css/sb-admin-2.css" rel="stylesheet"> -->
   <link href="https://raw.githubusercontent.com/noizwaves/bootstrap-social-buttons/v1.0.0/social-buttons-3.css" rel="stylesheet">
  <script type="text/javascript" src="/assets/jquery.min.js"></script>
 
</head>
<body>

  <header class="fixed-nav" id="main-nav">
    <div class="container">
      <div class="grid no-pad">
        <div class="logo-container"><a class="nav-btn" href="/brands"><i class="material-icons">menu</i></a>
          <div class="logo">Shopmonk</div>
          <div class="tagline" style="width: 867px;">The World's Best Brands. Here.</div>
        </div>
        <div class="nav-right">
          <ul class="icon-nav">
            <li><a class="nav-btn" href="/wish-list"><i class="material-icons">favorite</i></a></li>    
            <li><a class="nav-btn" href="/checkout"><i class="material-icons">shopping_cart</i></a></li>
            <li><a class="nav-btn" href="/profile"><i class="material-icons">account_box</i></a></li>
            <li class="has-dropdown shown" id="drop">
              <a href="#" class="nav-btn js-prevent-link">
                <i class="material-icons md-24">more_horiz</i>
              </a>
              <ul class="dropdown" hidden="">
                <li>
                  <a href="/about">About</a>
                </li>
                <li>
                  <a href="/privacy">Privacy Policy</a>
                </li>
                <li>
                  <a href="/terms">Terms</a>
                </li>
                <li>
                  <a href="/contact">Contact</a>
                </li>
                <li>
                  <a href="/logout">Logout</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>

  <div class="fixed-nav" id="secondary-nav" style="left: 0px;">
    <div class="container">
      <div class="grid title-bar">
        <a class="nav-btn" href="/">
          <i class="material-icons">arrow_back</i>
        </a>
        <div class="title-bar-container">
          <h2>Payment</h2>
        </div>
      </div>
    </div>
  </div>

  <div id="static-content">
    <section class="content static-page-container payment-page">
              <div class="row">

        <div class="col-2-3">
  <input type="hidden" name="order_id" value="61">
          
          <div class="panel panel-transparent payment-panel">
            <div class="panel-heading">
              <strong>Select Payment Option</strong>
            </div>
            <input type="hidden" name="_token" value="F18hoPNhvDzyODzHywSHuRxS3Yc9waQWeGhRgndR">
        
            <div class="panel-body">
              <div class="grid no-pad">
                <div class="row">
                  <div class="col-1-4 tab-nav-col">
                    <ul data-tabs="" class="tabs">
                                                                  <li class="active"><a data-tab="" href="#debit-credit" class="active">Debit &amp; Credit Card</a></li>
                                              <li class=""><a data-tab="" href="#fast-banking" class="">0% EMI - FastBanking</a></li>
                                              <li class=""><a data-tab="" href="#cash-care" class="">0% EMI - CashCare</a></li>
                                              <li class=""><a data-tab="" href="#cod" class="">Cash On Delivery</a></li>
                                                              </ul>
                  </div>

                  <div class="col-3-4 tab-content-col">
                    <div data-tabs-content="">
                                                                  <div data-tabs-pane="" class="tabs-pane active" id="debit-credit" data-tab-id="debit-credit">         
                        <form action="/payment/debit-credit" method="POST">
<input type="hidden" name="_token" value="F18hoPNhvDzyODzHywSHuRxS3Yc9waQWeGhRgndR">
<input type="hidden" name="order_id" value="61">
<input type="hidden" name="paymentGateway" value="debit-credit">
<button type="submit" onclick="return confirm('You will be redirected to payment gateway. Please click OK to proceed');" class="img-btn">text</button>
</form>

<h3>
  Debit &amp; Credit Card
</h3>

<ul>
  </ul><br>
 <p class="breakword">
  details
 </p>


               
                        </div>
                                              <div data-tabs-pane="" class="tabs-pane" id="fast-banking" data-tab-id="fast-banking">         
                        <form action="/payment/fast-banking" method="POST">
<input type="hidden" name="_token" value="F18hoPNhvDzyODzHywSHuRxS3Yc9waQWeGhRgndR">
<input type="hidden" name="order_id" value="61">
<input type="hidden" name="paymentGateway" value="fast-banking">
<button type="submit" onclick="return confirm('You will be redirected to payment gateway. Please click OK to proceed');" class="img-btn">text</button>
</form>

<h3>
  FastBanking EMI
</h3>

<ul>
  </ul><br>
 <p class="breakword">
  test details
 </p>


               
                        </div>
                                              <div data-tabs-pane="" class="tabs-pane" id="cash-care" data-tab-id="cash-care">         
                        <button type="submit" disabled="" onclick="return confirm('You will be redirected to payment gateway. Please click OK to proceed');" class="img-btn">pay with fast banking</button>

<h3>
  CashCare EMI
</h3>

<ul>
  </ul><br>
 <p class="breakword">
  FAST AND CONVENIENT
Select your preferred EMI option and provide your verification documents and enjoy your product.




 </p>


               
                        </div>
                                              <div data-tabs-pane="" class="tabs-pane" id="cod" data-tab-id="cod">         
                        <form action="/payment/cod" method="POST">
<input type="hidden" name="_token" value="F18hoPNhvDzyODzHywSHuRxS3Yc9waQWeGhRgndR">
<input type="hidden" name="order_id" value="61">
<input type="hidden" name="paymentGateway" value="cod">
<button type="submit" onclick="return confirm('Your order will be placed. Please click OK to proceed');" class="img-btn">text</button>
</form>

<h3>
  Cash on Delivery
</h3>

<ul>
  </ul><br>
 <p class="breakword">
  test details
 </p>


               
                        </div>
                                                              </div>
                  </div>
                </div>
              </div>

            </div><!-- panel -->

          </div>

        </div><!-- col 2-3 -->

        <div class="col-1-3">
          <div class="cart panel panel-transparent">
            <div class="panel-heading">
              Your Shopping Cart
              <span class="label-count label-transparent">1</span>
            </div>
            <div class="panel-body">
              <ul class="product-list"><li class="product-item"><div class="grid no-pad product-name-row"><span class="img-container"><img src="http://demo.shopmonk.com/uploads/products/thumbnails/2016062712480200000046558.jpg" '=""></span><span class="product-item-content"><h5 class="product-title">Biba Aurelia Solid Women's Straight Kurta Pink S</h5></span></div></li></ul>
            </div>
          </div>

          <div class="cart panel panel-transparent">
            <div class="panel-heading">
              Shipping Address
            </div>

                        <div class="panel-body">
              <div class="name">
                <address>
                  <strong>chaitanya kumar</strong><br>
                  <a href="mailto:#">admin@shopmonk.com</a>
                </address>

                <br>
              </div>

              <div class="address">
                <address>
                  dd<br>
                  bangalore, Maharashtra 560001<br>
                  <abbr title="Phone">P:</abbr> 8884768915
                </address>
              </div>
            </div>
                      </div>


        </div>
      </div>

    <script type="text/javascript" src="/assets/cart-list.js"></script>
   <script type="text/javascript" src="/assets/ajaxSetup.js"></script> 

 <script type="text/javascript">
/*
 function proceedToPayment(label) {

  //var $target = e.target;

$.ajax({
        url: "/payment/" + label,
        type:"POST",
        data: { order_id: $("[name='order_id']").val(), paymentGateway :  label  },
        success:function(data){    
          
        },error:function(){ 
            //alert("error!!!!");
        }
    }); 

}*/
 </script>
      </section>
  </div>

<script>
$("#drop").click(function(){
$(".dropdown").toggle();
});
</script>
<script type="text/javascript" src="/assets/static-a8dffa2cf58e8a61d7ec.min.js">
</script>
</body></html>