<html lang="en" class="js-tabby"><head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Shopmonk</title>
  <meta name="description" content="Shopmonk">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,500,600,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
   <link href="https://raw.githubusercontent.com/noizwaves/bootstrap-social-buttons/v1.0.0/social-buttons-3.css" rel="stylesheet">
  <script type="text/javascript" src="/assets/jquery.min.js"></script>
  <style>
  
  </style>
 </head>
<body>

  <header class="fixed-nav" id="main-nav">
    <div class="container">
      <div class="grid no-pad">
        <div class="logo-container"><a class="nav-btn" href="/brands"><i class="material-icons">menu</i></a>
          <div class="logo">Shopmonk</div>
          <div class="tagline" style="width: 867px;">The World's Best Brands. Here.</div>
        </div>
        <div class="nav-right">
          <ul class="icon-nav">
            <li><a class="nav-btn" href="/wish-list"><i class="material-icons">favorite</i></a></li>    
            <li><a class="nav-btn" href="/checkout"><i class="material-icons">shopping_cart</i></a></li>
            <li><a class="nav-btn" href="/profile"><i class="material-icons">account_box</i></a></li>
            <li class="has-dropdown shown" id="drop">
              <a href="#" class="nav-btn js-prevent-link">
                <i class="material-icons md-24">more_horiz</i>
              </a>
              <ul class="dropdown" hidden="">
                <li>
                  <a href="/about">About</a>
                </li>
                <li>
                  <a href="/privacy">Privacy Policy</a>
                </li>
                <li>
                  <a href="/terms">Terms</a>
                </li>
                <li>
                  <a href="/contact">Contact</a>
                </li>
                <li>
                  <a href="/logout">Logout</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>

  <div class="fixed-nav" id="secondary-nav" style="left: 0px;">
    <div class="container">
      <div class="grid title-bar">
        <a class="nav-btn" href="/">
          <i class="material-icons">arrow_back</i>
        </a>
        <div class="title-bar-container">
          <h2>Shipping Details</h2>
        </div>
      </div>
    </div>
  </div>

  <div id="static-content">
    <section class="content static-page-container payment-page">
            <form action="/address/store" method="POST" role="form">
      <input type="hidden" name="_token" value="F18hoPNhvDzyODzHywSHuRxS3Yc9waQWeGhRgndR">                          
      <div class="row">      
        <div class="col-sm-12 alert" style="width: 100%;">
                  </div>
        <div class="col-1-3">          
          <div class="form-group">
            <label>First Name</label>
            <input type="text" name="fname" value="" class="form-control" placeholder="John">
          </div>

          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" value="" class="form-control" placeholder="you@youremail.com">
          </div>

          <div class="form-group">
            <label>Address Line 1</label>
            <textarea style="width: 100%" name="address1" class="form-control" placeholder="Door #1, ABC Street"></textarea>
          </div>

          <div class="form-group">
            <label>City</label>
            <input type="text" name="city" value="" class="form-control" placeholder="New Delhi">
          </div>

          <div class="form-group">
            <label>State</label>
            <select class="form-control" name="state_id">
              <option value="" class="form-control" selected="selected">Select a State</option>
                            <option value="1" class="form-control"> Maharashtra</option> 
                          </select>
          </div>
          <div class="checkbox form-group">
            <label><input type="checkbox" name="tnc" value="1">I accept the Terms &amp; Conditions of shopmonk</label>
          </div>
        </div><!-- col 1-3 -->

        <div class="col-1-3">
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" name="lname" value="" class="form-control" placeholder="Doe">
          </div>

          <div class="form-group">
            <label>Mobile Number</label>
            <input type="text" name="phone" value="" class="form-control" placeholder="9999988888">
          </div>

          <div class="form-group">
            <label>Address Line 2</label>
            <textarea style="width: 100%" name="address2" class="form-control" placeholder="Door #1, ABC Street"></textarea>
          </div>

          <div class="form-group">
            <label>Zip Code</label>
            <input type="text" name="zipcode" value="" class="form-control" placeholder="605001">
          </div>

          <div class="form-group">
            <label>Landmark</label>
            <input type="text" name="landmark" value="" class="form-control" placeholder="Near ABC Bank">
          </div>

        </div><!-- col 1-3 -->

        <!-- Cart data -->
        <div class="col-1-3 Cartdata">
          <div class="cart panel panel-transparent">
            <div class="panel-heading">
              Your Shopping Cart
              <span class="label-count label-transparent">1</span>
            </div>
            <div class="panel-body">
              <ul class="product-list"><li class="product-item"><div class="grid no-pad product-name-row"><span class="img-container"><img src="http://demo.shopmonk.com/uploads/products/thumbnails/2016062712480200000046558.jpg" '=""></span><span class="product-item-content"><h5 class="product-title">Biba Aurelia Solid Women's Straight Kurta Pink S</h5></span></div></li></ul>
            </div>
          </div>
        </div>
        <!-- Cart data -->
        
      </div>

      <div class="row  action-group">
        <div class="col-3-3">
          <div class="form-group">
            <input type="submit" value="Continue to Payment" class="btn btn-block btn-dark">
          </div>
        </div>
      </div>
    </form>

<script src="http://demo.shopmonk.com/assets/js/cart-list.js"></script>
      </section>
  </div>

<script>
$("#drop").click(function(){
$(".dropdown").toggle();
});
</script>
 
<script type="text/javascript" src="/assets/static-a8dffa2cf58e8a61d7ec.min.js">
</script>
</body></html>