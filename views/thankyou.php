<html lang="en" class="js-tabby"><head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Shopmonk</title>
  <meta name="description" content="Shopmonk">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,500,600,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- <link href="http://demo.shopmonk.com/assets/css/frontend.css" rel="stylesheet">
   <link href="http://demo.shopmonk.com/assets/css/sb-admin-2.css" rel="stylesheet">
   --> <link href="https://raw.githubusercontent.com/noizwaves/bootstrap-social-buttons/v1.0.0/social-buttons-3.css" rel="stylesheet">
    <script type="text/javascript" src="/assets/jquery.min.js"></script>

  <style>
  
  </style>
 </head>
<body>

  <header class="fixed-nav" id="main-nav">
    <div class="container">
      <div class="grid no-pad">
        <div class="logo-container"><a class="nav-btn" href="/brands"><i class="material-icons">menu</i></a>
          <div class="logo">Shopmonk</div>
          <div class="tagline" style="width: 867px;">The World's Best Brands. Here.</div>
        </div>
        <div class="nav-right">
          <ul class="icon-nav">
            <li><a class="nav-btn" href="/wish-list"><i class="material-icons">favorite</i></a></li>    
            <li><a class="nav-btn" href="/checkout"><i class="material-icons">shopping_cart</i></a></li>
            <li><a class="nav-btn" href="/profile"><i class="material-icons">account_box</i></a></li>
            <li class="has-dropdown shown" id="drop">
              <a href="#" class="nav-btn js-prevent-link">
                <i class="material-icons md-24">more_horiz</i>
              </a>
              <ul class="dropdown" hidden="">
                <li>
                  <a href="/about">About</a>
                </li>
                <li>
                  <a href="/privacy">Privacy Policy</a>
                </li>
                <li>
                  <a href="/terms">Terms</a>
                </li>
                <li>
                  <a href="/contact">Contact</a>
                </li>
                <li>
                  <a href="/logout">Logout</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>

  <div class="fixed-nav" id="secondary-nav" style="left: 0px;">
    <div class="container">
      <div class="grid title-bar">
        <a class="nav-btn" href="/">
          <i class="material-icons">arrow_back</i>
        </a>
        <div class="title-bar-container">
          <h2></h2>
        </div>
      </div>
    </div>
  </div>

  <div id="static-content">
    <section class="content static-page-container payment-page">
              <div class="thank-you-panel panel panel-transparent">
        <div class="panel-heading">
          Confirmation &amp; Invoice
        </div>

        <div class="panel-body">
          <p class="lead">
                      Thank you for placing an order with Shopmonk. Your order was processed successfully and will be delivered shortly <!-- <strong>Wednesday, 20th April, 2016</strong> -->. You will receive the tracking details for this order shortly.
                    </p>
          <br>

          <p>
            In case of any questions/comments, please contact our support team at <a href="mailto:support@shopmonk.com">support@shopmonk.com</a>.
          </p>
        </div>

        <div class="order-invoice">

          <div class="panel">
            <div class="panel-heading">
              <strong>Order ID: #61</strong>
            </div>

            <div class="panel-body">
              <div class="name">
                <address>
                  <strong>admin</strong><br>
                  <a href="mailto:#">admin@shopmonk.com</a>
                </address>

                <br>
              </div>

              <div class="address">
                <address>
                  <strong>chaitanya kumar</strong><br>
                  dd<br>
                  <abbr title="Phone">P:</abbr> 8884768915
                </address>
              </div>
            </div>

            <div class="panel-footer">
              <ul class="product-list">
                                  <li class="product-item">
                    <div class="grid no-pad product-name-row">
                      <div class="row">
                        <div class="col-3-5">
                          <span class="img-container">
                            <img src="http://demo.shopmonk.com/uploads/products/thumbnails/2016062712510800000044657.jpg">
                          </span>

                          <span class="product-item-content">
                            <h5 class="product-title">
                              Biba-Aurelia Solid Women's Straight Kurta-Pink-S
                              <br>
                              <small>
                                (Tracking code will be available shortly)
                              </small>
                            </h5>
                          </span>
                        </div>

                        <div class="col-2-5">
                          <div class="grid no-pad price-component">
                            <div class="col-2-3 text-right">List Price</div>
                            <div class="col-1-3 text-right"><strong>INR 1200</strong></div>
                          </div>

                          <div class="grid no-pad price-component">
                            <div class="col-2-3 text-right">Quantity</div>
                            <div class="col-1-3 text-right"><strong>1</strong></div>
                          </div>

                          <div class="grid no-pad price-component">
                            <div class="col-2-3 text-right">Discount</div>
                            <div class="col-1-3 text-right"><strong>INR 0</strong></div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </li>
                              </ul>

              <hr>

              <div class="grid no-pad price-component">
                <div class="col-2-3 text-right">Total Price</div>
                <div class="col-1-3 text-right">INR 1200.00 </div>
              </div>
            </div>
          </div>


        </div>
      </div>

      <script type="text/javascript">

  $.ajax({           
            url: "http://demo.shopmonk.com/api/cart/destroy",
            type:"GET",
            data: {},
            success:function(data){          
                                   
            },error:function(){ 
              //$('#alert').html('update fail!'); 
            }
        });
  
        window.history.forward();
        function noBack() { window.history.forward(); }

        $(document).ready(function(){
          noBack();
        });
        $(document).on("pageshow",function(){
          if (event.persisted) noBack();
        });
      </script>
      </section>
  </div>

<script>
$("#drop").click(function(){
$(".dropdown").toggle();
});
</script>
<script type="text/javascript" src="/assets/static-a8dffa2cf58e8a61d7ec.min.js">
</script>
</body></html>