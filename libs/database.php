<?php
use Illuminate\Database\Capsule\Manager as Capsule;

$database = new Capsule;

$database->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'magento',
    'username'  => 'shopmonk',
    'password'  => 'psmithinthecity',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);



// Make this Capsule instance available globally via static methods... (optional)
$database->setAsGlobal();

