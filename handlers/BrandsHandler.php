<?php

namespace ShopMonk;

class BrandsHandler
{


public static function index()
{

echo '[
  {
    "id": 0,
    "name": "Abercrombie and Fitch",
    "slug": "abercrombie-and-fitch",
    "categories": [4, 1],
    "imageURL": "./brand-logos/abercrombie-and-fitch-logo.jpg"
  },

  {
    "id": 1,
    "name": "Gucci",
    "slug": "Gucci",
    "categories": [5, 3, 2, 4],
    "imageURL": "./brand-logos/gucci-logo.jpg"
  },

  {
    "id": 2,
    "name": "Banana Republic",
    "slug": "Banana Republic",
    "categories": [3, 4, 5, 0],
    "imageURL": "./brand-logos/banana-republic-logo.jpg"
  },

  {
    "id": 3,
    "name": "Brioni",
    "slug": "Brioni",
    "categories": [6, 3, 1],
    "imageURL": "./brand-logos/brioni-logo.jpg"
  },

  {
    "id": 4,
    "name": "Burberry",
    "slug": "Burberry",
    "categories": [0, 6],
    "imageURL": "./brand-logos/burberry-logo.jpg"
  },

  {
    "id": 5,
    "name": "Bvlgari",
    "slug": "Bvlgari",
    "categories": [5, 0, 3, 6, 1],
    "imageURL": "./brand-logos/bvlgari-logo.jpg"
  },

  {
    "id": 6,
    "name": "Calvin Klein",
    "slug": "Calvin Klein",
    "categories": [1, 5],
    "imageURL": "./brand-logos/calvin-klein-logo.jpg"
  },

  {
    "id": 7,
    "name": "Chanel",
    "slug": "Chanel",
    "categories": [6, 5, 3],
    "imageURL": "./brand-logos/chanel-logo.jpg"
  },

  {
    "id": 8,
    "name": "Chloe",
    "slug": "Chloe",
    "categories": [5, 2],
    "imageURL": "./brand-logos/chloe-logo.jpg"
  },

  {
    "id": 9,
    "name": "Chloe",
    "slug": "Chloe",
    "categories": [1, 6],
    "imageURL": "./brand-logos/christian-dior-logo.jpg"
  },

  {
    "id": 10,
    "name": "David Lawrence",
    "slug": "David Lawrence",
    "categories": [6, 5, 0, 4],
    "imageURL": "./brand-logos/david-lawrence-logo.jpg"
  },
  {
    "id": 11,
    "name": "Escada",
    "slug": "Escada",
    "categories": [6, 0, 4],
    "imageURL": "./brand-logos/escada-logo.jpg"
  }

]

';

}

public static function view($request)
{

  echo '[{"id":"19","cardType":"product","cardSize":"w1 h1","title":"Gionee P5 Mini(Black, 8 GB)","URL":"http:\/\/demo.shopmonk.com\/product\/gionee-p5-mini-black-8-gb-","brand":{"id":"1","name":"Gionee"},"category":{"id":"2","name":"Smart Phones"},"price":"5349.00","imageURL":"http:\/\/demo.shopmonk.com\/\/uploads\/products\/2016070410010100000063250.jpg"},{"id":"20","cardType":"product","cardSize":"w1 h1","title":"Gionee P5L(Gold, 16 GB)","URL":"http:\/\/demo.shopmonk.com\/product\/gionee-p5l-gold-16-gb-","brand":{"id":"1","name":"Gionee"},"category":{"id":"2","name":"Smart Phones"},"price":"53000.00","imageURL":"http:\/\/demo.shopmonk.com\/\/uploads\/products\/2016070410094100000032808.jpg"}]
';


}


}