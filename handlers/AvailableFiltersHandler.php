<?php 
namespace ShopMonk;
use Helpers\SoapRequester;
use Helpers\Func;
class AvailableFiltersHandler extends ApiHandler
{


public static function index()
{

 $data = new \stdClass;

 $categories = (new SoapRequester())->getCategories();

 $categories = Func::categoryTreeToSingle($categories['children']);

 $parnetCategory = array_values(array_filter($categories,function($value){
 
         return $value['parent_id']==1;
 
   }));



 $subCategory = array_values(array_filter($categories,function($value){

        return $value['parent_id']!=1;

  }));



$data->categories = self::categoryTransformer($parnetCategory);
$data->subcategories = self::subCategoryTransformer($subCategory);
$data->brands = self::brandTransformer();
$data->genders = self::genderTransformer();


return self::render($data);


// echo '{
//   "brands": [
//     {
//       "id": 0,
//       "name": "Abercrombie and Fitch",
//       "categories": [4, 1],
//       "subcategories": [2, 3]
//     },

//     {
//       "id": 1,
//       "name": "Gucci",
//       "categories": [5, 3, 2, 4],
//       "subcategories": [0, 1]
//     },

//     {
//       "id": 2,
//       "name": "Banana Republic",
//       "categories": [3, 4, 5, 0],
//       "subcategories": [4, 1]
//     },

//     {
//       "id": 3,
//       "name": "Brioni",
//       "categories": [6, 3, 1],
//       "subcategories": [0, 6]
//     },

//     {
//       "id": 4,
//       "name": "Burberry",
//       "categories": [0, 6],
//       "subcategories": [2, 5]
//     },

//     {
//       "id": 5,
//       "name": "Bvlgari",
//       "categories": [5, 0, 3, 6, 1],
//       "subcategories": [1, 2]
//     },

//     {
//       "id": 6,
//       "name": "Calvin Klein",
//       "categories": [1, 5],
//       "subcategories": [0, 6]
//     },

//     {
//       "id": 7,
//       "name": "Chanel",
//       "categories": [6, 5, 3],
//       "subcategories": [3, 4, 5, 6]
//     },

//     {
//       "id": 8,
//       "name": "Chloe",
//       "categories": [5, 2],
//       "subcategories": [0, 1, 2, 3]
//     },

//     {
//       "id": 9,
//       "name": "Christian Dior",
//       "categories": [1, 6],
//       "subcategories": [5, 2, 3]
//     },

//     {
//       "id": 10,
//       "name": "David Lawrence",
//       "categories": [6, 5, 0, 4],
//       "subcategories": [6, 2, 3]
//     }
//   ],

//   "categories": [
//     {
//       "id": 0,
//       "name": "Handbag",
//       "brands": [3, 1, 8],
//       "subcategories": [0, 1, 2]
//     },

//     {
//       "id": 1,
//       "name": "Cocktail Dresses",
//       "brands": [6, 4, 8, 7],
//       "subcategories": [2, 3]
//     },

//     {
//       "id": 2,
//       "name": "Make-up Kit",
//       "brands": [2, 6],
//       "subcategories": [1, 3]
//     },

//     {
//       "id": 3,
//       "name": "Perfumes",
//       "brands": [0, 5, 10, 4, 7, 2],
//       "subcategories": [5, 4]
//     },

//     {
//       "id": 4,
//       "name": "Necklaces",
//       "brands": [0, 7],
//       "subcategories": [2, 4]
//     },

//     {
//       "id": 5,
//       "name": "Evening Gown",
//       "brands": [9, 2, 1],
//       "subcategories": [6]
//     },

//     {
//       "id": 6,
//       "name": "Wallets",
//       "brands": [0, 6, 4, 9, 5],
//       "subcategories": [1, 3]
//     }
//   ],


//   "subcategories": [
//     {
//       "id": 0,
//       "name": "Top Handles & Boston Bags",
//       "brands": [0, 6, 4, 9, 5],
//       "categories": [1, 3]
//     },

//     {
//       "id": 1,
//       "name": "Totes",
//       "brands": [0, 6, 4, 9, 5],
//       "categories": [1, 3]
//     },

//     {
//       "id": 2,
//       "name": "Shoulder Bags",
//       "brands": [1, 4, 5, 9],
//       "categories": [1, 3]
//     },

//     {
//       "id": 3,
//       "name": "Hobos",
//       "brands": [10, 2, 4],
//       "categories": [1, 3]
//     },

//     {
//       "id": 4,
//       "name": "Crossbody Bags",
//       "brands": [3, 7, 9],
//       "categories": [1, 3]
//     },

//     {
//       "id": 5,
//       "name": "Backpacks",
//       "brands": [0, 5, 4],
//       "categories": [1, 3]
//     },

//     {
//       "id": 6,
//       "name": "Mini Bags",
//       "brands": [1, 6, 7, 8],
//       "categories": [1, 3]
//     }
//   ],

//   "genders": [
//     {
//       "id": 0,
//       "name": "Male"
//     },

//     {
//       "id": 1,
//       "name": "Women"
//     }
//   ]

// }
// ';

}

  private static function categoryTransformer($data)
  {


    return array_map(function($cat){


          return  [
              'id' => $cat['category_id'],
              'name' => $cat['name'],
              'brands' => [0, 6, 4, 9, 5],
              'subcategories' => [0, 6, 4, 9, 5],
            ];


    },$data);

    
  }

  private static function subCategoryTransformer($data)
  {
      return array_map(function($cat){

               return   [
                    'id' => $cat['category_id'],
                    'name' => $cat['name'],
                    'brands' => [0, 6, 4, 9, 5],
                    'categories' => [0, 6, 4, 9, 5],
                  ];


          },$data);

  }


  private static function genderTransformer(){

      return [

          [
      "id"=> 0,
      "name"=> "Male"
    ],

    [
      "id"=> 1,
      "name"=> "Women"
    ]

      ];
    
  }

  private static function brandTransformer(){

      return [

        [  
          "id"=> 0,
          "name"=> "Abercrombie and Fitch",
          "categories"=> [4, 1],
          "subcategories"=> [2, 3]
        

        ]



      ];

  }



}