<?php
namespace ShopMonk;

/**
* products api handler
*/
class ProductsApiHandler
{
	
	
	public static function allProducts()
	{
		  $SiteURL = API_URL;
        $callbackUrl = CALLBACK_URL;
        $temporaryCredentialsRequestUrl = $callbackUrl . "/oauth/initiate?oauth_callback=" . urlencode($callbackUrl);
        $adminAuthorizationUrl = $SiteURL . '/admin/oauth_authorize';
        $accessTokenRequestUrl = $SiteURL . '/oauth/token';
        $apiUrl = $SiteURL . '/api/rest/';
        $consumerKey = CONSUMER_KEY; //'cexbd2tw1e31a0zviviz4vu8q6sh3r1s';
        $consumerSecret = CONSUMER_SECRET; //'cexbd2tw1e31a0zviviz4vu8q6sh3r1s';

        if (!isset($_GET['oauth_token']) && isset($_SESSION['state']) && $_SESSION['state'] == 1) {
            $_SESSION['state'] = 0;
        }
        try {
            $authType = (isset($_SESSION['state']) && $_SESSION['state'] == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;
            $oauthClient = new \OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
            $oauthClient->enableDebug();


            if (!isset($_GET['oauth_token']) && !isset($_SESSION['state'])) {
                $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl);

                $_SESSION['secret'] = isset($requestToken['oauth_token_secret']) ? $requestToken['oauth_token_secret'] : null ;
                $_SESSION['state'] = 1;
                // header('Location: ' . $adminAuthorizationUrl . '?oauth_token=' . $requestToken['oauth_token']);
                $_SESSION['token'] = isset($_SESSION['token']) ? $_SESSION['token'] : null ;
                $oauthClient->setToken($_SESSION['token'], $_SESSION['secret']);
                $resourceUrl = "$apiUrl/products?page=3&limit=50";
                $oauthClient->fetch($resourceUrl,array(),'GET', array('Content-Type' => 'application/xml', 'Accept' => '*/*'));
                $productsList = json_decode($oauthClient->getLastResponse(),true);
                
                $transformerData = array();

                
                


                echo json_encode($transformerData);
                
                exit;
            } else if ($_SESSION['state'] == 1) {
                $oauthClient->setToken($_GET['oauth_token'], $_SESSION['secret']);
                $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl);
                $_SESSION['state'] = 2;
                $_SESSION['token'] = $accessToken['oauth_token'];
                $_SESSION['secret'] = $accessToken['oauth_token_secret'];
                header('Location: ' . $callbackUrl);
                exit;
            } else {
              
                $oauthClient->setToken($_SESSION['token'], $_SESSION['secret']);
                $resourceUrl = "$apiUrl/products";
                $oauthClient->fetch($resourceUrl,array(),'GET', array('Content-Type' => 'application/xml', 'Accept' => '*/*'));
                $productsList = json_decode($oauthClient->getLastResponse(),true);
                //print_r($productsList);
                echo json_encode(self::transformerData($productsList));
            }
        } catch (OAuthException $e) {
            print_r($e);
        } 
	}

  public static function singleProduct()
  {
    //$slug = $_GET['slug'];

    echo '{
  "breadcrumbs": [
    {
      "name": "Gucci",
      "URL": "/brands/gucci"
    },

    {
      "name": "Bags",
      "URL": "/category/bags"
    },

    {
      "name": "Shoulder Bags",
      "URL": "/category/shoulder-bags"
    }
  ],
  "product":{
      "id": 1,
      "name": "Bags"
  },
  "currentVariant": {
    "id": 0,
    "title": "Sylviv Leather Mini Chain Bag - Black Leather",
    "brand": {
      "id": 0,
      "name": "Gucci"
    },
    "category": {
      "id": 0,
      "name": "Bags"
    },
    "priceBeforeDiscount": 87000,
    "formattedPriceBeforeDiscount": "INR 87,000",

    "price": 85000,
    "formattedPrice": "INR 85,000",

    "priceMeta": "6% Discount + Free Shipping",

    "description": "The sliding chain strap can be worn multiple ways, changing between a shoulder and a top handle bag. Made in our fine grain leather, noted for its shine and softness.",
    "thumbnailURL": "./gucci-bag/bag-black/small/1.jpg",
    "images": [
      {
        "thumbnailURL": "./gucci-bag/bag-black/small/1.jpg",
        "imageURL": "./gucci-bag/bag-black/small/1.jpg"
      },

      {
        "thumbnailURL": "./gucci-bag/bag-black/small/2.jpg",
        "imageURL": "./gucci-bag/bag-black/small/2.jpg"
      },

      {
        "thumbnailURL": "./gucci-bag/bag-black/small/3.jpg",
        "imageURL": "./gucci-bag/bag-black/small/3.jpg"
      },

      {
        "thumbnailURL": "./gucci-bag/bag-black/small/4.jpg",
        "imageURL": "./gucci-bag/bag-black/small/4.jpg"
      },

      {
        "thumbnailURL": "./gucci-bag/bag-black/small/5.jpg",
        "imageURL": "./gucci-bag/bag-black/small/5.jpg"
      },

      {
        "thumbnailURL": "./gucci-bag/bag-black/small/6.jpg",
        "imageURL": "./gucci-bag/bag-black/small/6.jpg"
      },

      {
        "thumbnailURL": "./gucci-bag/bag-black/small/7.jpg",
        "imageURL": "./gucci-bag/bag-black/small/7.jpg"
      }

    ],
    "specifications": [
      "Pink and hibiscus red leather",
      "Key with leather holder",
      "Camel microfiber lining with a suede-like finish",
      "Sliding chain strap can be worn as a shoulder strap with 20\" drop or can be worn as  handle with 11\" drop",
      "Interior open pocket",
      "Exterior pocket",
      "Lock closure",
      "Small",
      "8\"W x 5\"H x 3\"D",
      "Made in Italy",
      "Gold metal hardware"
    ]
  },

  "variants": [
    {
      "type": "Styles",
      "list": [

        {
          "id": 0,
          "title": "Sylviv Leather Mini Chain Bag - Black Leather",
          "priceBeforeDiscount": 87000,
          "formattedPriceBeforeDiscount": "INR 87,000",

          "price": 85200,
          "formattedPrice": "INR 85,200",
          "priceMeta": "6% Discount + Free Shipping",
          "description": "A small structured bag with a key lock closure. The sliding chain strap can be worn multiple ways, changing between a shoulder and a top handle bag. Made in our fine grain leather, noted for its shine and softness.",
          "thumbnailURL": "./gucci-bag/thumb/black-leather-thumb.jpg",
          "images": [
            {
              "thumbnailURL": "./gucci-bag/bag-black/small/1.jpg",
              "imageURL": "./gucci-bag/bag-black/small/1.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-black/small/2.jpg",
              "imageURL": "./gucci-bag/bag-black/small/2.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-black/small/3.jpg",
              "imageURL": "./gucci-bag/bag-black/small/3.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-black/small/4.jpg",
              "imageURL": "./gucci-bag/bag-black/small/4.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-black/small/5.jpg",
              "imageURL": "./gucci-bag/bag-black/small/5.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-black/small/6.jpg",
              "imageURL": "./gucci-bag/bag-black/small/6.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-black/small/7.jpg",
              "imageURL": "./gucci-bag/bag-black/small/7.jpg"
            }

          ],

          "brand": {
            "id": 0,
            "name": "Gucci"
          },
          "category": {
            "id": 0,
            "name": "Bags"
          },
          "specifications": [
            "Pink and hibiscus red leather",
            "Key with leather holder",
            "Camel microfiber lining with a suede-like finish",
            "Sliding chain strap can be worn as a shoulder strap with 20\" drop or can be worn as  handle with 11\" drop",
            "Interior open pocket",
            "Exterior pocket",
            "Lock closure",
            "Small",
            "8\"W x 5\"H x 3\"D",
            "Made in Italy",
            "Gold metal hardware"
          ]
        },

        {
          "id": 1,
          "title": "Sylviv Leather Mini Chain Bag - Hibiscus Red Leather",
          "priceBeforeDiscount": 87000,
          "formattedPriceBeforeDiscount": "INR 87,000",
          "price": 86000,
          "formattedPrice": "INR 86,000",
          "priceMeta": "6% Discount + Free Shipping",
          "description": "Made in our fine grain leather, noted for its shine and softness. A small structured bag with a key lock closure. The sliding chain strap can be worn multiple ways, changing between a shoulder and a top handle bag.",
          "thumbnailURL": "./gucci-bag/thumb/red-leather-thumb.jpg",
          "images": [
            {
              "thumbnailURL": "./gucci-bag/bag-red/small/1.jpg",
              "imageURL": "./gucci-bag/bag-red/small/1.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-red/small/2.jpg",
              "imageURL": "./gucci-bag/bag-red/small/2.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-red/small/3.jpg",
              "imageURL": "./gucci-bag/bag-red/small/3.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-red/small/4.jpg",
              "imageURL": "./gucci-bag/bag-red/small/4.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-red/small/5.jpg",
              "imageURL": "./gucci-bag/bag-red/small/5.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-red/small/6.jpg",
              "imageURL": "./gucci-bag/bag-red/small/6.jpg"
            }

          ],

          "brand": {
            "id": 0,
            "name": "Gucci"
          },
          "category": {
            "id": 0,
            "name": "Bags"
          },
          "specifications": [
            "Pink and hibiscus red leather",
            "Key with leather holder",
            "Camel microfiber lining with a suede-like finish",
            "Sliding chain strap can be worn as a shoulder strap with 20\" drop or can be worn as  handle with 11\" drop",
            "Interior open pocket",
            "Exterior pocket",
            "Lock closure",
            "Small",
            "8\"W x 5\"H x 3\"D",
            "Made in Italy",
            "Gold metal hardware"
          ]
        },

        {
          "id": 2,
          "title": "Sylviv Leather Mini Chain Bag - Pink Leather",
          "priceBeforeDiscount": 87000,
          "formattedPriceBeforeDiscount": "INR 87,000",
          "price": 86000,
          "formattedPrice": "INR 86,000",
          "priceMeta": "6% Discount + Free Shipping",
          "description": "Made in our fine grain leather, noted for its shine and softness. A small structured bag with a key lock closure. The sliding chain strap can be worn multiple ways, changing between a shoulder and a top handle bag.",
          "thumbnailURL": "./gucci-bag/thumb/pink-leather-thumb.jpg",
          "images": [
            {
              "thumbnailURL": "./gucci-bag/bag-pink/small/1.jpg",
              "imageURL": "./gucci-bag/bag-pink/small/1.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-pink/small/2.jpg",
              "imageURL": "./gucci-bag/bag-pink/small/2.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-pink/small/3.jpg",
              "imageURL": "./gucci-bag/bag-pink/small/3.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-pink/small/4.jpg",
              "imageURL": "./gucci-bag/bag-pink/small/4.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-pink/small/5.jpg",
              "imageURL": "./gucci-bag/bag-pink/small/5.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-pink/small/6.jpg",
              "imageURL": "./gucci-bag/bag-pink/small/6.jpg"
            },

            {
              "thumbnailURL": "./gucci-bag/bag-pink/small/7.jpg",
              "imageURL": "./gucci-bag/bag-pink/small/7.jpg"
            }

          ],

          "brand": {
            "id": 0,
            "name": "Gucci"
          },
          "category": {
            "id": 0,
            "name": "Bags"
          },
          "specifications": [
            "Pink and hibiscus red leather",
            "Key with leather holder",
            "Camel microfiber lining with a suede-like finish",
            "Sliding chain strap can be worn as a shoulder strap with 20\" drop or can be worn as  handle with 11\" drop",
            "Interior open pocket",
            "Exterior pocket",
            "Lock closure",
            "Small",
            "8\"W x 5\"H x 3\"D",
            "Made in Italy",
            "Gold metal hardware"
          ]
        }
      ]
    }
  ],


  "currentModifier": {
    "id": 0,
    "title": "Small"
  },

  "modifiers": [
    {
      "type": "Sizes",
      "list": [
        {
          "id": 0,
          "label": "S",
          "available": true
        },
        {
          "id": 1,
          "label": "M",
          "available": true
        },
        {
          "id": 2,
          "label": "L",
          "available": false
        }
      ]
    }
  ],

  "options": [
    {
      "type": "Additional Options",
      "list": [
        {
          "id": 0,
          "title": "Shipping",
          "price": 0,
          "formattedPrice": "Free",
          "selected": true,
          "editable": false
        },

        {
          "id": 1,
          "title": "Get 2nd year Extended Warranty",
          "price": 2500,
          "formattedPrice": "INR 2,500",
          "selected": false,
          "editable": true
        },

        {
          "id": 2,
          "title": "Get 1 year Accidental Damage Protection",
          "price": 1500,
          "formattedPrice": "INR 1,500",
          "selected": false,
          "editable": true
        }
      ]

    }
  ],

  "similarProducts": [
    {
      "id": 0,
      "title": "Sylviv Leather Mini Chain Bag - Black Leather",
      "price": 85000,
      "formattedPrice": "INR 85,000",
      "thumbnailURL": "./gucci-bag/bag-black/small/1.jpg"
    },

    {
      "id": 1,
      "title": "Sylviv Leather Mini Chain Bag - Pink Leather",
      "price": 85000,
      "formattedPrice": "INR 85,000",
      "thumbnailURL": "./gucci-bag/bag-pink/small/1.jpg"
    },

    {
      "id": 2,
      "title": "Sylviv Leather Mini Chain Bag - Red Leather",
      "price": 85000,
      "formattedPrice": "INR 85,000",
      "thumbnailURL": "./gucci-bag/bag-red/small/2.jpg"
    },

    {
      "id": 3,
      "title": "Sylviv Leather Mini Chain Bag - Blue Leather",
      "price": 85000,
      "formattedPrice": "INR 85,000",
      "thumbnailURL": "./gucci-bag/bag-black/small/4.jpg"
    }

  ]

}
';

  }


private static function transformerData($productsList)
{
$transformerData = [];
    foreach ($productsList as $key => $value) {
                    
                    $transformerData[] = [

                        'id' => $value['entity_id'],
                        'cardSize' => "w1 h1",
                        'title' => $value['name'],
                        'cardType' =>  "product",
                        'URL' => '/prodcut/'.$value['url_key'],
                        'brand' => [
                            'id' => 0,
                            'name' => 'JIMMY CHOO'
                        ],'category' => [
                            'id' => 0,
                            'name' => 'JIMMY CHOO'
                        ],
                        'price' =>  isset($value['price']) ? $value['price'] : 00 ,
                        'imageURL' => 'http://shopmonk.herokuapp.com/e68af3562682d6bc05528ba3d27856fc.jpg'

                    ];

                  }
                  return $transformerData;
}


}