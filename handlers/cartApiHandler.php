<?php
namespace ShopMonk;

/**
* cart api handler
*/
class cartApiHandler
{
	
	
	
  public static function getCart()
  {
    

    echo '{
  "total": {
    "price": 85000,
    "formattedPrice": "INR 85,000"
  },

  "products": [
    {
      "id": 1,
      "title": "Sylviv Leather Mini Chain Bag - Black Leather",
      "brand": {
        "id": 0,
        "name": "Gucci"
      },
      "category": {
        "id": 0,
        "name": "Bags"
      },

      "discount": {
        "id": 0,
        "percentage": 10,
        "price": 800,
        "formattedPrice": "800"
      },

      "selectedOptions": [
        {
          "id": 0,
          "title": "Shipping",
          "price": 0,
          "formattedPrice": "0"
        },

        {
          "id": 1,
          "title": "Extended Warranty",
          "price": 2500,
          "formattedPrice": "2,500"
        }
      ],

      "listPrice": 87000,
      "formattedListPrice": "87,000",

      "subTotalPrice": 85000,
      "formattedSubTotalPrice": "INR 85,000",

      "thumbnailURL": "http://placehold.it/48x48"
    }
  ]

}
';

  }



public static function postCart(){

  echo '{"data":{"error":0,"message":"product added to cart successfully"}}';

}


public static function destroyCart(){

  echo '{"data":{"error":0,"message":"cart destroyed successfully "}}';
}

public static function removeCart(){

  //$rowid = $_GET['rowid'];

  echo '{"data":{"error":0,"message":"cart destroyed successfully "}}';
}



}