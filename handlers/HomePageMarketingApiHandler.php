<?php 
namespace ShopMonk;
use Illuminate\Database\Capsule\Manager as Capsule;


class HomePageMarketingApiHandler
{


		public static function index()
	{


$obj = new \stdClass;
$obj->showBanner = false;
$obj->showSlider = true;


if($banner = Capsule::table('cms_block')->select('title','content')->where('identifier','=','toppromo')->first())
{
     $obj->showBanner = true;    
    $obj->bannerContent = self::bannerTransformer($banner);

}

$sliders =self::SliderTransformer(Capsule::table('banners')->get());
$obj->slides = $sliders;



echo json_encode($obj,true);


}


  private static function bannerTransformer($banner){

    $data = [];
     preg_match("/{{(.*?)}}/", $banner->content, $matches);

          if(!empty($matches))
            {
              $data['buttonURL'] = $matches[1];
              $banner->content = str_replace($matches[0], '', $banner->content);
        }
          else
            $data['buttonURL'] = null;

          $data['title'] = $banner->title;
          $data['content'] = $banner->content;

          return $data;
    
  }

  private static function SliderTransformer($sliders){

    $data = [];
  
    foreach($sliders as $slider){
        $data[]=[

              "imageURL" => IMAGE_ROOT_PATH.$slider->bannerimage,
              "heading" => $slider->title,
              "subtitle"=> $slider->content,
              "linkURL"=>  $slider->link

        ];  
    }


        return $data;
    
  }



}