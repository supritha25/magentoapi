<?php
namespace Helpers;

/**
* Soap requester
* Sreej32h
*/
class SoapRequester
{
	
	private $client,$session;
	
	function __construct()
	{
		$this->session= null;
		$this->client = new \SoapClient(SOAP_ROOT_URL);
		return $this;
	}


	public function login()
	{
		$this->session = $this->client->login(SOAP_USER_NAME,SOAP_AUTH_TOKEN);
		return $this;
	}


	public function getCategories(){

	if($this->session===null)
		self::login();

	return $this->client->call($this->session, 'catalog_category.tree',true);


	}



}