<?php
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $route) {

//user api routes
$route->addRoute('GET', '/api/user', 'UserApiHandler@index');
$route->addRoute('POST', '/api/post_user', 'UserApiHandler@save');
$route->addRoute('GET', '/api/home-page-marketing', 'HomePageMarketingApiHandler@index');

//category api routes
$route->addRoute('GET', '/api/categories/{slug}', 'ProductsCategoryApiHandler@singleCategory');
$route->addRoute('GET', '/api/categories', 'ProductsCategoryApiHandler@index');

$route->addRoute('GET', '/api/subcategories', 'ProductsCategoryApiHandler@subcategories');
//brands api routes
$route->addRoute('GET', '/api/brand/{slug}', 'BrandsHandler@view');
$route->addRoute('GET', '/api/brands', 'BrandsHandler@index');


// //filters
$route->addRoute('GET', '/api/available-filters', 'AvailableFiltersHandler@index');

// //products routes 
$route->addRoute('GET','/api/products','ProductsApiHandler@allProducts');
$route->addRoute('GET','/api/single-product','ProductsApiHandler@singleProduct');


// //cart routes
$route->addRoute('GET','/api/cart','cartApiHandler@getCart');
$route->addRoute('POST','/api/cart','cartApiHandler@postCart');
$route->addRoute('GET','/api/cart/destroy','cartApiHandler@destroyCart');
$route->addRoute('GET','/api/cart/remove','cartApiHandler@removeCart');

// //wishlist routes
$route->addRoute('GET','/api/wishlist','wishlistApiHandler@wishlist');
$route->addRoute('GET','/api/wishlist/add','wishlistApiHandler@addWishlist');
$route->addRoute('GET','/api/wishlist/remove','wishlistApiHandler@removeWishlist');


// //page routes
$route->addRoute('GET','/api/page','pageViewHandler@page'); 


$route->addRoute('GET','/login','pageViewHandler@login');
$route->addRoute('GET','/test','pageViewHandler@test');
$route->addRoute('GET','/personal-details','pageViewHandler@personaldetails'); 
$route->addRoute('GET','/paymentpage','pageViewHandler@paymentpage');
$route->addRoute('GET','/thankyou','pageViewHandler@thankyou'); 





// //default view    
 $route->addRoute('GET', '[/{default}]', 'pageViewHandler@defaultView');





});
